<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 3/28/19
 * Time: 3:29 PM
 */

namespace Rbm\Data\CustomerData\Plugin;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;

class SectionConfigConverter
{
    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * SectionConfigConverter constructor.
     *
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig
    ) {
        $this->scopeConfig = $scopeConfig;
    }

    public function afterConvert(\Magento\Customer\CustomerData\SectionConfigConverter $subject, $result)
    {
        unset($result['sections']['rbm_data_noaction_route']);

        $route = $this->scopeConfig->getValue(
            'rbm/general/invalidation_route',
            ScopeInterface::SCOPE_STORE
        );

        if (in_array($route, array_keys($result['sections']))) {
            $result['sections'][$route][] = "a_rule_section";
        } else {
            $result['sections'][$route] = ["a_rule_section"];
        }

        return $result;
    }
}
