<?php

namespace Rbm\Data\CustomerData;

use Magento\Customer\CustomerData\SectionSourceInterface;
use Magento\Customer\Model\Session as CustomerSession;
use Magento\Framework\DataObject;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Rbm\Data\Api\RelationRepositoryInterface;
use Magento\Framework\Api\FilterBuilder;
use Magento\Framework\Api\Search\FilterGroupBuilder;

use Magento\Customer\Model\Group as CustomerGroups;

/**
 * Class RuleSection
 */
class RuleSection extends DataObject implements SectionSourceInterface
{
    /** @var CustomerSession */
    private $session;

    /** @var RelationRepositoryInterface */
    protected $relationRepository;

    /** @var \Magento\Framework\Api\SearchCriteriaBuilder */
    private $searchCriteriaBuilder;

    /** @var \Magento\Framework\Api\FilterBuilder */
    protected $filterBuilder;

    /** @var FilterGroupBuilder */
    private $filterGroupBuilder;

    /**
     * RuleSection constructor.
     *
     * @param CustomerSession $session
     * @param RelationRepositoryInterface $relationRepository
     * @param FilterBuilder $filterBuilder
     * @param FilterGroupBuilder $filterGroupBuilder
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     */
    public function __construct(
        CustomerSession $session,
        RelationRepositoryInterface $relationRepository,
        FilterBuilder $filterBuilder,
        FilterGroupBuilder $filterGroupBuilder,
        SearchCriteriaBuilder $searchCriteriaBuilder
    ) {
        $this->session = $session;
        $this->relationRepository = $relationRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->filterBuilder = $filterBuilder;
        $this->filterGroupBuilder = $filterGroupBuilder;
    }

    public function getSectionData()
    {
        $default = [
                'id' => $this->session->getCustomerId(),
                'name' => $this->session->getCustomer()->getName(),
                'email' => $this->session->getCustomer()->getEmail()
            ];

        $data = [];
        $customerGroup = null;
        if ($this->session->isLoggedIn()) {
            $customerGroup = $this->session->getCustomerGroupId();
            $data = $this->getActiveRelations($customerGroup);
        }

        $data = array_merge($data, $default);

        return $data;
    }

    /**
     * @param null $customerGroup
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getActiveRelations($customerGroup = null)
    {
        $filters = [];
        $filters[] = $this->filterBuilder
            ->setField('customer_group')
            ->setValue(CustomerGroups::CUST_GROUP_ALL)
            ->setConditionType('eq')
            ->create();

        if ($customerGroup) {
            $filters[] = $this->filterBuilder
                    ->setField('customer_group')
                    ->setValue($customerGroup)
                    ->setConditionType('eq')
                    ->create();
        }

        $filterByCustomerGroup = $this->filterGroupBuilder->setFilters($filters)->create();
        $filterByActivity = $this->filterGroupBuilder->setFilters(
            [
                $this->filterBuilder
                    ->setField('is_active')
                    ->setValue('1')
                    ->setConditionType('eq')
                    ->create()
            ]
        )->create();

        $this->searchCriteriaBuilder->setFilterGroups([$filterByCustomerGroup, $filterByActivity]);

        $relations = $this->relationRepository->getList($this->searchCriteriaBuilder->create())->getItems();

        $data = [];
        foreach ($relations as $key => $relation) {
            $data['rules'][$key]['name'] = $relation->getName();
            $data['rules'][$key]['private_data'] = $relation->getPrivateData();
            $data['rules'][$key]['public_page'] = $relation->getPublicPage();
        }

        return $data;
    }
}
