<?php

namespace Rbm\Data\Model;

use Rbm\Data\Api\Data;
use Rbm\Data\Api\RelationRepositoryInterface;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Rbm\Data\Model\ResourceModel\Relation as ResourceRelation;
use Rbm\Data\Model\ResourceModel\Relation\CollectionFactory as RelationCollectionFactory;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class RelationRepository
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class RelationRepository implements RelationRepositoryInterface
{
    /**
     * @var ResourceRelation
     */
    protected $resource;

    /**
     * @var RelationFactory
     */
    protected $relationFactory;

    /**
     * @var RelationCollectionFactory
     */
    protected $relationCollectionFactory;

    /**
     * @var Data\RelationSearchResultsInterfaceFactory
     */
    protected $searchResultsFactory;

    /**
     * @var DataObjectHelper
     */
    protected $dataObjectHelper;

    /**
     * @var DataObjectProcessor
     */
    protected $dataObjectProcessor;

    /**
     * @var \Rbm\Data\Api\Data\RelationInterfaceFactory
     */
    protected $dataPageFactory;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var CollectionProcessorInterface
     */
    private $collectionProcessor;

    /**
     * @param ResourceRelation $resource
     * @param RelationFactory $relationFactory
     * @param Data\RelationInterfaceFactory $dataRelationFactory
     * @param RelationCollectionFactory $relationCollectionFactory
     * @param Data\RelationSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     * @param CollectionProcessorInterface $collectionProcessor
     */
    public function __construct(
        ResourceRelation $resource,
        RelationFactory $relationFactory,
        Data\RelationInterfaceFactory $dataRelationFactory,
        RelationCollectionFactory $relationCollectionFactory,
        Data\RelationSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor = null
    ) {
        $this->resource = $resource;
        $this->relationFactory = $relationFactory;
        $this->relationCollectionFactory = $relationCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataRelationFactory = $dataRelationFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
        $this->collectionProcessor = $collectionProcessor ?: $this->getCollectionProcessor();
    }

    /**
     * Save Relation data
     *
     * @param \Rbm\Data\Api\Data\RelationInterface $relation
     * @return Relation
     * @throws CouldNotSaveException
     */
    public function save(\Rbm\Data\Api\Data\RelationInterface $relation)
    {
        if ($relation->getStoreId() === null) {
            $storeId = $this->storeManager->getStore()->getId();
            $relation->setStoreId($storeId);
        }
        try {
            $this->resource->save($relation);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(
                __('Could not save the page: %1', $exception->getMessage()),
                $exception
            );
        }
        return $relation;
    }

    /**
     * Load Relation data by given Relation Identity
     *
     * @param string $relationId
     * @return Relation
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getById($relationId)
    {
        $relation = $this->relationFactory->create();
        $relation->load($relationId);
        if (!$relation->getId()) {
            throw new NoSuchEntityException(__('The CMS page with the "%1" ID doesn\'t exist.', $relationId));
        }
        return $relation;
    }

    /**
     * Load Relation data collection by given search criteria
     *
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     * @param \Magento\Framework\Api\SearchCriteriaInterface $criteria
     * @return \Rbm\Data\Api\Data\RelationSearchResultsInterface
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $criteria)
    {
        /** @var \Rbm\Data\Model\ResourceModel\Relation\Collection $collection */
        $collection = $this->relationCollectionFactory->create();

        $this->collectionProcessor->process($criteria, $collection);

        /** @var Data\RelationSearchResultsInterface $searchResults */
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        $searchResults->setItems($collection->getItems());
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * Delete Relation
     *
     * @param \Rbm\Data\Api\Data\RelationInterface $relation
     * @return bool
     * @throws CouldNotDeleteException
     */
    public function delete(\Rbm\Data\Api\Data\RelationInterface $relation)
    {
        try {
            $this->resource->delete($relation);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the relation: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * Delete relation by given relation Identity
     *
     * @param string $relationId
     * @return bool
     * @throws CouldNotDeleteException
     * @throws NoSuchEntityException
     */
    public function deleteById($relationId)
    {
        return $this->delete($this->getById($relationId));
    }

    /**
     * Retrieve collection processor
     *
     * @return CollectionProcessorInterface
     */
    private function getCollectionProcessor()
    {
        if (!$this->collectionProcessor) {
            $this->collectionProcessor = \Magento\Framework\App\ObjectManager::getInstance()->get(
                //'Rbm\Data\Model\Api\SearchCriteria\RelationCollectionProcessor'
                \Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface::class
            );
        }
        return $this->collectionProcessor;

    }
}
