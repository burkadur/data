<?php

/**
 * Relation.php
 *
 * @copyright Copyright © 2019 Rbm. All rights reserved.
 * @author    bruvinsky@gmail.com
 */

namespace Rbm\Data\Model;

use Magento\Framework\Model\AbstractModel;
use Magento\Framework\DataObject\IdentityInterface;
use Rbm\Data\Api\Data\RelationInterface;

class Relation extends AbstractModel implements IdentityInterface, RelationInterface
{
    /**
     * CMS page cache tag
     */
    const CACHE_TAG = 'rbm_data_relation';

    /**
     * @var string
     */
    protected $_cacheTag = 'rbm_data_relation';

    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'rbm_data_relation';

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->_init('Rbm\Data\Model\ResourceModel\Relation');
    }

    /**
     * Get identities
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * Save from collection data
     *
     * @param array $data
     * @return $this|bool
     */
    public function saveCollection(array $data)
    {
        if (isset($data[$this->getId()])) {
            $this->addData($data[$this->getId()]);
            $this->getResource()->save($this);
        }
        return $this;
    }

    public function getId()
    {
        return parent::getData(self::ENTITY_ID);
    }

    public function getName()
    {
        return parent::getData(self::NAME);
    }

    public function getPrivateData()
    {
        return parent::getData(self::PRIVATE_DATA);
    }

    public function getPublicPage()
    {
        return parent::getData(self::PUBLIC_PAGE);
    }

    public function getCustomerGroup()
    {
        return parent::getData(self::CUSTOMER_GROUP);
    }

    public function getStoreviews()
    {
        return parent::getData(self::STOREVIEWS);
    }

    public function getFrom()
    {
        return parent::getData(self::FROM);
    }

    public function getTo()
    {
        return parent::getData(self::TO);
    }

    public function isActive()
    {
        return (bool)parent::getData(self::IS_ACTIVE);
    }

    /**
     * Set ID
     *
     * @param int $id
     * @return \Rbm\Data\Api\Data\RelationInterface
     */
    public function setId($id)
    {
        return $this->setData(self::ENTITY_ID, $id);
    }

    /**
     * Set name
     *
     * @param string $name
     * @return \Rbm\Data\Api\Data\RelationInterface
     */
    public function setName($name)
    {
        return $this->setData(self::NAME, $name);
    }

    /**
     * Set private data
     *
     * @param string $privateData
     * @return \Rbm\Data\Api\Data\RelationInterface
     */
    public function setPrivateData($privateData)
    {
        return $this->setData(self::PRIVATE_DATA, $privateData);
    }

    /**
     * Set public page
     *
     * @param string $publicPage
     * @return \Rbm\Data\Api\Data\RelationInterface
     */
    public function setPublicPage($publicPage)
    {
        return $this->setData(self::PUBLIC_PAGE, $publicPage);
    }

    /**
     * Set customer group
     *
     * @param string $customerGroup
     * @return \Rbm\Data\Api\Data\RelationInterface
     */
    public function setCustomerGroup($customerGroup)
    {
        return $this->setData(self::CUSTOMER_GROUP, $customerGroup);
    }

    /**
     * Set storeviews
     *
     * @param string $storeviews
     * @return \Rbm\Data\Api\Data\RelationInterface
     */
    public function setStoreviews($storeviews)
    {
        return $this->setData(self::STOREVIEWS, $storeviews);
    }

    
    /**
     * Set from
     *
     * @param string $from
     * @return \Rbm\Data\Api\Data\RelationInterface
     */
    public function setFrom($from)
    {
        return $this->setData(self::FROM, $from);
    }

    /**
     * Set to
     *
     * @param string $to
     * @return \Rbm\Data\Api\Data\RelationInterface
     */
    public function setTo($to)
    {
        return $this->setData(self::TO, $to);
    }

    /**
     * Set is active
     *
     * @param int|bool $isActive
     * @return \Magento\Cms\Api\Data\PageInterface
     */
    public function setIsActive($isActive)
    {
        return $this->setData(self::IS_ACTIVE, $isActive);
    }
}
