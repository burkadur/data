<?php

namespace Rbm\Data\Model\Source;

use Magento\Framework\Data\OptionSourceInterface;

/**
 * @api
 */
interface PageSourceInterface extends OptionSourceInterface
{

}
