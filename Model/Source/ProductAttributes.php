<?php

namespace Rbm\Data\Model\Source;

use Magento\Catalog\Model\Product;

class ProductAttributes implements ProductAttributesSourceInterface
{
    /** @var Product  */
    protected $_productModel;

    /**
     * ProductAttributes constructor.
     *
     * @param Product $productModel
     */
    public function __construct(Product $productModel) {
        $this->_productModel = $productModel;
    }

    /**
     * Return array of pages to rule_section output
     *
     * @return array
     */
    public function toOptionArray()
    {
        $productAttributes = $this->_productModel->getAttributes();
        $productAttribute = [];
        foreach ($productAttributes as $attribute) {
            $productAttribute[] = [
                'value' => $attribute->getAttributeCode(),
                'label' => $attribute->getFrontendLabel()
            ];
        }
        return $productAttribute;
    }
}
