<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Rbm\Data\Model\Source;

class Page implements PageSourceInterface
{
    const ALL_PAGES      = 32000;
    const LISTING_PAGE   = 1;
    const PDP_PAGE       = 2;
    const CHECKOUT_PAGE  = 3;

    /**
     * Return array of pages to rule_section output
     *
     * @return array
     */
    public function toOptionArray()
    {
        $pages = [
            [
                'label' => __('ALL Pages'),
                'value' => (string)self::ALL_PAGES,
            ],
            [
                'label' => __('Listing'),
                'value' => (string)self::LISTING_PAGE,
            ],
            [
                'label' => __('PDP'),
                'value' => (string)self::PDP_PAGE,
            ],
            [
                'label' => __('Checkout'),
                'value' => (string)self::CHECKOUT_PAGE,
            ]
        ];

        return $pages;
    }
}
