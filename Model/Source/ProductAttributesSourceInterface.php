<?php

namespace Rbm\Data\Model\Source;

use Magento\Framework\Data\OptionSourceInterface;

/**
 * @api
 */
interface ProductAttributesSourceInterface extends OptionSourceInterface
{

}
