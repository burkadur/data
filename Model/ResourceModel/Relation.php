<?php

/**
 * Relation.php
 *
 * @copyright Copyright © 2019 Rbm. All rights reserved.
 * @author    bruvinsky@gmail.com
 */
namespace Rbm\Data\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Magento\Framework\Model\AbstractModel;

/**
 * Class Relation
 */
class Relation extends AbstractDb
{
    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('rbm_data_relation', 'entity_id');
    }

}
