<?php
/**
 * Collection.php
 *
 * @copyright Copyright © 2019 Rbm. All rights reserved.
 * @author    bruvinsky@gmail.com
 */
namespace Rbm\Data\Model\ResourceModel\Relation;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'entity_id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Rbm\Data\Model\Relation', 'Rbm\Data\Model\ResourceModel\Relation');
    }
}
