<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 3/26/19
 * Time: 3:01 PM
 */

namespace Rbm\Data\Api;

interface RelationRepositoryInterface
{
    /**
     * Save Relation.
     *
     * @param Rbm\Data\Api\Data\RelationInterface $relation
     * @return \Rbm\Data\Api\Data\RelationInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(\Rbm\Data\Api\Data\RelationInterface $relation);

    /**
     * Retrieve Relation.
     *
     * @param int $relationId
     * @return \Rbm\Data\Api\Data\RelationInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($relationId);

    /**
     * Retrieve Relations matching the specified criteria.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Rbm\Data\Api\Data\RelationSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);

    /**
     * Delete Relation.
     *
     * @param \Rbm\Data\Api\Data\RelationInterface $relation
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(\Rbm\Data\Api\Data\RelationInterface $relation);

    /**
     * Delete Relation by ID.
     *
     * @param int $relationId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($relationId);
}