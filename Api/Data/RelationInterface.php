<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 3/26/19
 * Time: 3:02 PM
 */

namespace Rbm\Data\Api\Data;


interface RelationInterface
{

    /**#@+
     * Constants for keys of data array. Identical to the name of the getter in snake case
     */
    const ENTITY_ID             = 'entity_id';
    const NAME                  = 'name';
    const PRIVATE_DATA          = 'private_data';
    const PUBLIC_PAGE           = 'public_page';
    const CUSTOMER_GROUP        = 'customer_group';
    const STOREVIEWS            = 'storeviews';
    const FROM                  = 'from';
    const TO                    = 'to';
    const IS_ACTIVE             = 'is_active';
    /**#@-*/

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getId();

    /**
     * Get name
     *
     * @return string|null
     */
    public function getName();

    /**
     * Get private data
     *
     * @return string|null
     */
    public function getPrivateData();

    /**
     * Get public page
     *
     * @return string|null
     */
    public function getPublicPage();

    /**
     * Get customer group
     *
     * @return string|null
     */
    public function getCustomerGroup();

    /**
     * Get storeviews
     *
     * @return string
     */
    public function getStoreviews();


    /**
     * Get from
     *
     * @return string|null
     */
    public function getFrom();

    /**
     * Get to
     *
     * @return string|null
     */
    public function getTo();

    /**
     * Is active
     *
     * @return bool|null
     */
    public function isActive();

    /**
     * Set ID
     *
     * @param int $id
     * @return \Rbm\Data\Api\Data\RelationInterface
     */
    public function setId($id);

    /**
     * Set name
     *
     * @param string $name
     * @return \Rbm\Data\Api\Data\RelationInterface
     */
    public function setName($name);

    /**
     * Set private data
     *
     * @param string $privateData
     * @return \Rbm\Data\Api\Data\RelationInterface
     */
    public function setPrivateData($privateData);

    /**
     * Set public page
     *
     * @param string $publicPage
     * @return \Rbm\Data\Api\Data\RelationInterface
     */
    public function setPublicPage($publicPage);

    /**
     * Set customer group
     *
     * @param string $customerGroup
     * @return \Rbm\Data\Api\Data\RelationInterface
     */
    public function setCustomerGroup($customerGroup);

    /**
     * Set storeviews
     *
     * @param string $storeviews
     * @return \Rbm\Data\Api\Data\RelationInterface
     */
    public function setStoreviews($storeviews);

    /**
     * Set from
     *
     * @param string $from
     * @return \Rbm\Data\Api\Data\RelationInterface
     */
    public function setFrom($from);

    /**
     * Set to
     *
     * @param string $to
     * @return \Rbm\Data\Api\Data\RelationInterface
     */
    public function setTo($to);

    /**
     * Set is active
     *
     * @param int|bool $isActive
     * @return \Magento\Cms\Api\Data\PageInterface
     */
    public function setIsActive($isActive);
}
