<?php

namespace Rbm\Data\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;


interface RelationSearchResultsInterface extends SearchResultsInterface
{
    /**
     * Get Relations list.
     *
     * @return \Rbm\Data\Api\Data\RelationInterface[]
     */
    public function getItems();

    /**
     * Set Relations list.
     *
     * @param \Rbm\Data\Api\Data\RelationInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
